const sum = (stn1, stn2) => {
    let array1 = stn1.split("").reverse();
    let array2 = stn2.split("").reverse();
  
    let result = "";
    let carry = 0;
  
    for (let i = 0; i < Math.max(array1.length, array2.length); i++) {
      let num1 = parseInt(array1[i]) || 0;
      let num2 = parseInt(array2[i]) || 0;
      let sum = num1 + num2 + carry;
      result = (sum % 10).toString() + result;
      console.log(`Step ${i + 1}: ${num1} + ${num2} + ${carry} = ${sum} `);
      carry = Math.floor(sum / 10);
      console.log(`Result: ${result} , Carry: ${carry}`);
    }
  
    if (carry > 0) {
      result = carry.toString() + result;
    }
  
    return `Final Result: ${result}`;
  };
  
  console.log(sum("4223", "897"));
  //or
  // let stn1 = "123";
  // let stn2 = "4563";
  // console.log(sum(stn1, stn2));
  